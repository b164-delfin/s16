
//While Loop

let count2 =1;

while(count2 !== 6) {

	console.log(count2)
	count2++;
}


//Do While Loop

/*
Syntax:
	do {
		statement
	} while(expression/condition)



*/

let doWhileCounter = 1;

do {
	console.log(doWhileCounter);
	doWhileCounter++;
} while (doWhileCounter <= 20)


let number =Number(prompt("Give me a number"));

do{
	console.log("Do while:" + number);
	number += 1;
} while(number < 10);


//Do While Loop


// For Loop
/*
It consists of three parts:
1. Initialization - value that will track the progression of the loop
2. Expression/Condition - that will be evaluated which will determine whether the loop will run one more time.
3. FinalExpression - indicates how to advance the loop (++,--)

Syntax:
	for(initialization; expression/condition; finalExpression)



*/

for(let count = 0; count <= 20; count++) {
	console.log(count);
}

//accessing array items -for loops

let fruits =["Apple", "Durian", "Kiwi", "Pineapple", "Mango","Orange"];

console.log(fruits[2]);
console.log(fruits.length);
// to check the last item in an array
console.log(fruits[fruits.length-1]);

//showing all the items in an array using for loop

for (let index = 0; index < fruits.length; index++)  {
	console.log(fruits[index]);
}

//accessing elements of a string

let myString = "alex";
//.length is also a property used in strings
console.log(myString.length);

console.log(myString[0])

//For Loop

//Mini Activity

let faveCountries = ["USA", "Canada", "Australia", "UK", "Spain", "France"]

for (fave = 0; fave < faveCountries.length-1;fave++)
{
	console.log(faveCountries[fave]);
}

//Mini Activity

let num1 = parseInt(prompt("Enter a number:"));

console.log(`The number you provided is ${num1}`);

for (let count = num1; count >= 0;count--) {
	
	if(count <= 50) 
	 	{
		console.log(`The current value is ${count}. Terminating the loop.`); 
		break;
		} else if (count % 10 === 0) {
			console.log("The number is divisble by 10. Skipping the number.");
			continue;
		} else if (count % 5 === 0) {
			console.log(count);
		}
	
	}


	let string = "supercalifragilisticexpialidocious";
	console.log(string);
	let filteredString = " ";

	for(let i=0; i < string.length; i++){

		if (
			string[i].toLowerCase() == "a" ||
			string[i].toLowerCase() == "e" ||
			string[i].toLowerCase() == "i" ||
			string[i].toLowerCase() == "o" ||
			string[i].toLowerCase() == "u" ||

	



			){
			continue;
		} else {

			filteredString += string[i];
		}

	}

console.log(filteredString);